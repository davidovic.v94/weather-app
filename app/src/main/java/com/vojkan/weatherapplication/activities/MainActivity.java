package com.vojkan.weatherapplication.activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.tabs.TabLayout;
import com.vojkan.weatherapplication.R;
import com.vojkan.weatherapplication.adapters.ViewPagerAdapter;
import com.vojkan.weatherapplication.data.DataManage;
import com.vojkan.weatherapplication.fragments.WeatherFragment;
import com.vojkan.weatherapplication.fragments.LoadingDialog;
import com.vojkan.weatherapplication.network.BaseTask;
import com.vojkan.weatherapplication.network.TaskRunner;
import com.vojkan.weatherapplication.utils.DateConverter;
import com.vojkan.weatherapplication.utils.ImageManager;
import com.vojkan.weatherapplication.utils.LocationDataManager;
import com.vojkan.weatherapplication.weatherdata.City;
import com.vojkan.weatherapplication.weatherdata.Temperature;
import com.vojkan.weatherapplication.weatherdata.Weather;

import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private CoordinatorLayout coordinatorLayout;
    public static ViewPager viewPager;
    private LinearLayout linearLayoutMainWeatherInfo;
    private TextView textViewToolbarCityName, textViewToolbarWeatherDesc, textViewMainTemperature, textViewToday,
            textViewTemperatureRangeMax, textViewTemperatureRangeMin;
    private LocationManager locationManager;
    private LocationListener locationListener;
    public static ViewPagerAdapter viewPagerAdapter;
    public static List<Weather> weatherList = new ArrayList<>();
    public static List<String> weatherStringList = new ArrayList<>();
    public static List<City> places = new ArrayList<>();
    public static List<LatLng> locations = new ArrayList<>();
    public static SharedPreferences sharedPreferences;
    public static String currentFragmentTitle = null, locationFragmentTitle = null;
    private LoadingDialog loadingDialog;
    private AppBarLayout appBarLayout;

    private final TaskRunner taskRunner = new TaskRunner();
    private DataManage dataManage;
    private ImageManager imageManager;
    private LocationDataManager locationDataManager;

    public static boolean celsiusActivated = true;

//    private final String URL_CITY_NAME_METRIC = "https://api.openweathermap.org/data/2.5/weather?q=" + city.getCityName() + "&appid=" + API_KEY + "&units=metric";
//    private final String URL_CITY_NAME_COUNTRY_CODE_METRIC = "https://api.openweathermap.org/data/2.5/weather?q=" + city.getCityName() + "," + city.getCountry().getCountryId() + "&appid=" + API_KEY + "&units=metric";
//    private final String URL_LON_LAT_METRIC = "https://api.openweathermap.org/data/2.5/forecast?lat=" + city.getCoordinates().getLatitude() + "&lon=" + city.getCoordinates().getLongitude() + "&appid=" + API_KEY + "&units=metric";
//    private final String URL_ZIP_CODE_COUNTRY_CODE_METRIC = "https://api.openweathermap.org/data/2.5/weather?zip=" + city.getPostalCode() + city.getCountry().getCountryId() + "&appid=" + API_KEY + "&units=metric";
//    private final String URL_CITY_NAME_IMPERIAL = "https://api.openweathermap.org/data/2.5/weather?q=" + city.getCityName() + "&appid=" + API_KEY + "&units=imperial";
//    private final String URL_CITY_NAME_COUNTRY_CODE_IMPERIAL = "https://api.openweathermap.org/data/2.5/weather?q=" + city.getCityName() + "," + city.getCountry().getCountryId() + "&appid=" + API_KEY + "&units=imperial";
//    private final String URL_LON_LAT_IMPERIAL = "https://api.openweathermap.org/data/2.5/forecast?lat=" + city.getCoordinates().getLatitude() + "&lon=" + city.getCoordinates().getLongitude() + "&appid=" + API_KEY + "&units=imperial";
//    private final String URL_ZIP_CODE_COUNTRY_CODE_IMPERIAL = "https://api.openweathermap.org/data/2.5/weather?zip=" + city.getPostalCode() + city.getCountry().getCountryId() + "&appid=" + API_KEY + "&units=imperial";

    public static final String API_KEY = "5465e088656873aa1b0ee36b1e885a36";
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadingDialog = new LoadingDialog(this);



        Toolbar toolbar = findViewById(R.id.custom_toolbar);
        setSupportActionBar(toolbar);

        coordinatorLayout = findViewById(R.id.main_layout);
        viewPager = findViewById(R.id.view_pager);
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        appBarLayout = findViewById(R.id.app_bar_layout);
        linearLayoutMainWeatherInfo = findViewById(R.id.main_weather_info);
        MaterialButton showPlacesButton = findViewById(R.id.button_show_places);
        MaterialButton openWeatherApiButton = findViewById(R.id.button_open_weather_api);
        textViewToolbarCityName = findViewById(R.id.toolbar_city_name);
        textViewToolbarWeatherDesc = findViewById(R.id.toolbar_weather_description);
//        imageViewToolbarWeatherIcon = findViewById(R.id.toolbar_weather_icon);
        textViewMainTemperature = findViewById(R.id.text_view_main_temperature);
        textViewToday = findViewById(R.id.text_view_today);
        textViewTemperatureRangeMax = findViewById(R.id.temperature_range_max_temperature);
        textViewTemperatureRangeMin = findViewById(R.id.temperature_range_min_temperature);

        imageManager = new ImageManager();
        locationDataManager = new LocationDataManager(getApplicationContext());

        sharedPreferences = getSharedPreferences("com.vojkan.weatherapplication", Context.MODE_PRIVATE);
        dataManage = new DataManage(sharedPreferences);

        appBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) ->
                linearLayoutMainWeatherInfo.setAlpha((float) (appBarLayout.getTotalScrollRange() + verticalOffset) / appBarLayout.getTotalScrollRange()));

        tabLayout.setupWithViewPager(viewPager, true);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), 0);
        viewPager.setAdapter(viewPagerAdapter);

        showPlacesButton.setOnClickListener(onShowPlacesButtonClickListener);

        openWeatherApiButton.setOnClickListener(onOpemWeatherApiButtonClickListener);

        viewPager.addOnPageChangeListener(onPageChangeListener);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {

            @Override
            public void onLocationChanged(@NonNull Location location) {
                if (locationDataManager.isNewLocation(location, locations)) {
                    City city = locationDataManager.getCityFromLatLon(location.getLatitude(), location.getLongitude());
                    if (city != null) {
                        locationDataManager.removeLocationFromLists(places, locations, weatherList, weatherStringList, locationFragmentTitle);

                        LatLng latLng = new LatLng(city.getCoordinates().getLatitude(), city.getCoordinates().getLongitude());
                        locations.add(0, latLng);

                        if (currentFragmentTitle == null || currentFragmentTitle.equals(locationFragmentTitle)) {
                            currentFragmentTitle = city.getCityName();
                        }
                        locationFragmentTitle = city.getCityName();
                        dataManage.saveData();

                        if (viewPagerAdapter.getCount() > 0) {
                            viewPagerAdapter.removeFragment(0);
                            viewPagerAdapter.notifyDataSetChanged();

                            places.add(0, city);
                            loadingDialog.startDialog();
                            findWeatherForCity(city, 0);
                        } else {
                            dataManage.loadData();
                            locationDataManager.getCitiesFromLocations(places, locations);

                            if (locations != null && locations.size() > 0) {
                                while (weatherList.size() < locations.size()) weatherList.add(null);
                                while (weatherStringList.size() < locations.size())
                                    weatherStringList.add(null);
                            }

                            if (places != null && places.size() > 0) {
                                loadingDialog.startDialog();
                                for (int i = 0; i < places.size(); i++) {
                                    findWeatherForCity(places.get(i), i);
                                }
                            }
                        }
                    }
                } else {
                    if (places != null && places.size() > 0) {
                        loadingDialog.startDialog();
                        for (int i = 0; i < places.size(); i++) {
                            findWeatherForCity(places.get(i), i);
                        }
                    }
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(@NonNull String provider) {

            }

            @Override
            public void onProviderDisabled(@NonNull String provider) {

            }
        };

    }

    private final View.OnClickListener onShowPlacesButtonClickListener = view -> {
        Intent intent = new Intent(getApplicationContext(), PlacesActivity.class);
        startActivity(intent);
    };

    private final View.OnClickListener onOpemWeatherApiButtonClickListener = view -> {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://openweathermap.org/api"));
        startActivity(intent);
    };

    private final ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onPageSelected(int position) {
            Log.i("onPageSelected", viewPagerAdapter.getFragmentTitle(position));
            currentFragmentTitle = viewPagerAdapter.getFragmentTitle(position);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("currentFragmentTitle", currentFragmentTitle).apply();
            for (Weather weather : weatherList) {
                if (weather != null) {
                    if (weather.getCity() != null) {
                        if (weather.getCity().getCityName().equals(viewPagerAdapter.getFragmentTitle(position))) {
                            setAppBarLayoutInfo(weather);
                            return;
                        }
                    }
                }
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

    };

    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.title_location_permission)
                        .setMessage(R.string.text_location_permission)
                        .setPositiveButton(R.string.ok, (dialogInterface, i) ->
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION))
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 50, locationListener);
                }
            } else {
                Log.i("onRequestPermisR", "Ne radi");
                finishAffinity();
                finish();
            }
        }
    }

    private void findWeatherForCity(City city, int index) {
        final String URL_LON_LAT_METRIC_TEST = "https://api.openweathermap.org/data/2.5/onecall?lat=" + city.getCoordinates().getLatitude() + "&lon=" + city.getCoordinates().getLongitude() + "&exclude=minutely&appid=" + API_KEY + "&units=metric";
        taskRunner.executeAsync(new Task(URL_LON_LAT_METRIC_TEST, index));
    }

    private class Task extends BaseTask<String> {
        String stringUrl;
        int cityIndex;

        Task(final String stringUrl, int cityIndex) {
            this.stringUrl = stringUrl;
            this.cityIndex = cityIndex;
        }

        @Override
        public void setUiForLoading() {
            super.setUiForLoading();
        }

        @Override
        public String call() {
            HttpURLConnection connection = null;
            URL url;

            try {
                url = new URL(stringUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-type", "application/json");

                InputStream in = connection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int dataInputStream = reader.read();
                StringBuilder stringBuilder = new StringBuilder();

                while (dataInputStream != -1) {
                    char current = (char) dataInputStream;
                    stringBuilder.append(current);
                    dataInputStream = reader.read();
                }

                return stringBuilder.toString();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }

        @Override
        public void setDataAfterLoading(String result) {
            try {
                Log.i("Result", result);
                JSONObject jsonObject = new JSONObject(result);
                Weather weather = new Weather(jsonObject);
                weather.setCity(places.get(cityIndex));

                if (!celsiusActivated) {
                    weather.getTemperature().toFahrenheit();
                }

                weatherList.set(cityIndex, weather);
                weatherStringList.set(cityIndex, result);

                Log.i("Weather", weather.toString());

                if (viewPagerAdapter.getCount() == 0) {
                    if (weatherList.size() > 0 && places.size() > 0 && weatherStringList.size() > 0) {
                        if (weatherList.size() == places.size() && weatherStringList.size() == places.size()) {
                            if (!weatherList.contains(null) && !weatherStringList.contains(null)) {
                                for (int i = 0; i < weatherList.size(); i++) {
                                    addFragment(weatherList.get(i), weatherStringList.get(i));
                                }
                            }
                        }
                    }
                } else {
                    addFragment(weather, result);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void addFragment(Weather weather, String result) {
        WeatherFragment weatherFragment = WeatherFragment.newInstance(result, weather.getCity().getCityName());

        if (weather.getCity().getCityName().equals(locationFragmentTitle)) {
            viewPagerAdapter.addFragment(weatherFragment, weather.getCity().getCityName(), 0);
        } else {
            viewPagerAdapter.addFragment(weatherFragment, weather.getCity().getCityName());
        }

        viewPagerAdapter.notifyDataSetChanged();
        Log.i("Weather Add fragmen", weather.getCity().getCityName());

        if (viewPagerAdapter.getCount() == places.size() && currentFragmentTitle != null) {
            setCurrentFragment();
        }

    }

    private void setCurrentFragment() {
        for (int i = 0; i < viewPagerAdapter.getCount(); i++) {
            Log.i("checkForCurrent123", viewPagerAdapter.getFragmentTitle(i));
            if (viewPagerAdapter.getFragmentTitle(i).equals(currentFragmentTitle)) {
                viewPager.setCurrentItem(i, true);
                for (Weather weather : weatherList) {
                    if (weather.getCity().getCityName().equals(currentFragmentTitle)) {
                        setAppBarLayoutInfo(weather);
                        loadingDialog.dismissDialog();
                        return;
                    }
                }
            }
        }
    }

    private void setAppBarLayoutInfo(Weather weather) {
        if (appBarLayout.getVisibility() == View.INVISIBLE) {
            appBarLayout.setVisibility(View.VISIBLE);
        }
        textViewToolbarCityName.setText(weather.getCity().getCityName());
        textViewToolbarWeatherDesc.setText(weather.getWeatherDetails().getDescription());
        coordinatorLayout.setBackgroundResource(imageManager.setBackground(weather.getWeatherDetails().getIcon()));
        textViewToday.setText(DateConverter.getDayFromDate(OffsetDateTime.parse(weather.getTime().getTimeOfCalculation())));
        textViewMainTemperature.setText(String.format(Locale.getDefault(), "%d%s",
                weather.getTemperature().getCurrent().intValue(), Temperature.unit));
        textViewTemperatureRangeMax.setText(String.format(Locale.getDefault(), "%d%s", weather.getTemperature().getMax().intValue(),
                getResources().getString(R.string.temperature_char_degree)));
        textViewTemperatureRangeMin.setText(String.format(Locale.getDefault(), "%d%s", weather.getTemperature().getMin().intValue(),
                getResources().getString(R.string.temperature_char_degree)));
    }

    @Override
    protected void onResume() {
        super.onResume();

        dataManage.loadData();
        locationDataManager.getCitiesFromLocations(places, locations);

        if (locations != null && locations.size() > 0) {
            while (weatherList.size() < locations.size()) weatherList.add(null);
            while (weatherStringList.size() < locations.size()) weatherStringList.add(null);
        }
        if (checkLocationPermission()) {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 15000, 500, locationListener);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (viewPagerAdapter.getCount() > 0) {
            int i = viewPagerAdapter.getCount() - 1;
            while (i >= 0) {
                viewPagerAdapter.removeFragment(i);
                i--;
            }
            viewPagerAdapter.notifyDataSetChanged();
            appBarLayout.setVisibility(View.INVISIBLE);
            coordinatorLayout.setBackgroundResource(R.color.quantum_white_100);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(locationListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismissDialog();
            }
        }
    }

    @Override
    public void onBackPressed(){
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

}