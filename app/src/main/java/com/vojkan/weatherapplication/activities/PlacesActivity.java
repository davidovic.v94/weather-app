package com.vojkan.weatherapplication.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.vojkan.weatherapplication.R;
import com.vojkan.weatherapplication.adapters.WeatherListAdapter;
import com.vojkan.weatherapplication.data.DataManage;
import com.vojkan.weatherapplication.fragments.SearchDialog;
import com.vojkan.weatherapplication.utils.LocationDataManager;
import com.vojkan.weatherapplication.weatherdata.Weather;

import java.util.ArrayList;
import java.util.List;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class PlacesActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    public static WeatherListAdapter adapter;

    private MaterialButton buttonChangeTempUnit;
    private final List<Weather> citiesForDeletion = new ArrayList<>();
    private DataManage dataManage;
    private LocationDataManager locationDataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        buttonChangeTempUnit = findViewById(R.id.button_change_temperature_unit);
        MaterialButton buttonAddPlace = findViewById(R.id.button_add_place);
        dataManage = new DataManage(MainActivity.sharedPreferences);
        locationDataManager = new LocationDataManager(getApplicationContext());

        if (!MainActivity.weatherList.isEmpty()) {
            adapter = new WeatherListAdapter(MainActivity.weatherList);
            recyclerView.setAdapter(adapter);
        }

        if (MainActivity.celsiusActivated) {
            buttonChangeTempUnit.setText(R.string.temperature_unit_celsius);
        } else {
            buttonChangeTempUnit.setText(R.string.temperature_unit_fahrenheit);
        }

        adapter.setOnItemClickListener(onItemClickListener);

        itemTouchHelper.attachToRecyclerView(recyclerView);

        buttonAddPlace.setOnClickListener(onButtonAddPlaceClickListener);

        buttonChangeTempUnit.setOnClickListener(onButtonChangeTempUnitClickListener);

    }

    WeatherListAdapter.OnItemClickListener onItemClickListener = position -> {
        MainActivity.currentFragmentTitle = MainActivity.weatherList.get(position).getCity().getCityName();
        SharedPreferences.Editor editor = MainActivity.sharedPreferences.edit();
        editor.putString("currentFragmentTitle", MainActivity.currentFragmentTitle).apply();

        if (citiesForDeletion.size() > 0) {
            for(Weather weather : citiesForDeletion) {
                locationDataManager.removeLocation(weather.getCity(), MainActivity.locations);
            }
            MainActivity.currentFragmentTitle = MainActivity.locationFragmentTitle;
            dataManage.saveData();
        }

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    };

    ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

        @Override
        public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
            if (viewHolder.getAdapterPosition() == 0) return 0;
            return super.getMovementFlags(recyclerView, viewHolder);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            if (direction == ItemTouchHelper.LEFT) {
                Weather forDeletion = MainActivity.weatherList.get(position);
                citiesForDeletion.add(forDeletion);
                MainActivity.weatherList.remove(position);
                adapter.notifyItemRemoved(position);
                Snackbar snackbar = Snackbar.make(recyclerView, String.format("Deleting %s...", forDeletion.getCity().getCityName()), Snackbar.LENGTH_LONG);
                snackbar.setAction("Undo", view -> {
                    MainActivity.weatherList.add(position, forDeletion);
                    adapter.notifyItemInserted(position);
                    citiesForDeletion.remove(forDeletion);
                });
                snackbar.show();
            }
        }

        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

            new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                    .addSwipeLeftBackgroundColor(ContextCompat.getColor(PlacesActivity.this, R.color.quantum_googred))
                    .addSwipeLeftLabel("Delete")
                    .addSwipeLeftActionIcon(R.drawable.ic_baseline_delete_24)
                    .create()
                    .decorate();

            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }

    });

    View.OnClickListener onButtonAddPlaceClickListener = view -> {
        if (citiesForDeletion.size() > 0) {
            for(Weather weather : citiesForDeletion) {
                locationDataManager.removeLocation(weather.getCity(), MainActivity.locations);
                locationDataManager.removeCity(weather.getCity(), MainActivity.places);
            }
            MainActivity.currentFragmentTitle = MainActivity.locationFragmentTitle;
            dataManage.saveData();
        }
        DialogFragment dialogFragment = new SearchDialog();
        dialogFragment.show(this.getSupportFragmentManager(), "search_dialog");
    };

    View.OnClickListener onButtonChangeTempUnitClickListener = view -> {
        SharedPreferences.Editor editor = MainActivity.sharedPreferences.edit();
        if (MainActivity.celsiusActivated) {
            MainActivity.celsiusActivated = false;
            buttonChangeTempUnit.setText(R.string.temperature_unit_fahrenheit);
            for (Weather weather : MainActivity.weatherList) {
                weather.getTemperature().toFahrenheit();
            }
        } else {
            MainActivity.celsiusActivated = true;
            buttonChangeTempUnit.setText(R.string.temperature_unit_celsius);
            for (Weather weather : MainActivity.weatherList) {
                weather.getTemperature().toCelsius();
            }
        }
        adapter.notifyDataSetChanged();
        editor.putBoolean("celsiusActivated", MainActivity.celsiusActivated).apply();
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (citiesForDeletion.size() > 0) {
            for(Weather weather : citiesForDeletion) {
                locationDataManager.removeLocation(weather.getCity(), MainActivity.locations);
            }
            MainActivity.currentFragmentTitle = MainActivity.locationFragmentTitle;
            dataManage.saveData();
        }
        getSupportFragmentManager().popBackStack();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

}