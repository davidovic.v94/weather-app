package com.vojkan.weatherapplication.data;

import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;
import com.vojkan.weatherapplication.activities.MainActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DataManage implements Storage {

    SharedPreferences sharedPreferences;

    public DataManage(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void saveData() {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        try {
            List<String> latitudes = new ArrayList<>();
            List<String> longitude = new ArrayList<>();

            for (LatLng ll : MainActivity.locations) {
                latitudes.add(Double.toString(ll.latitude));
                longitude.add(Double.toString(ll.longitude));
            }

            editor.putString("currentFragmentTitle", MainActivity.currentFragmentTitle).apply();
            editor.putString("locationFragmentTitle", MainActivity.locationFragmentTitle).apply();
            editor.putString("latitudes", ObjectSerializer.serialize((Serializable) latitudes)).apply();
            editor.putString("longitude", ObjectSerializer.serialize((Serializable) longitude)).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadData() {
        try {
            List<String> latitudes;
            List<String> longitude;

            MainActivity.weatherList.clear();
            MainActivity.weatherStringList.clear();
            MainActivity.places.clear();
            MainActivity.locations.clear();
            MainActivity.currentFragmentTitle = null;
            MainActivity.locationFragmentTitle = null;

            MainActivity.currentFragmentTitle = sharedPreferences.getString("currentFragmentTitle", null);
            MainActivity.locationFragmentTitle = sharedPreferences.getString("locationFragmentTitle", null);
            MainActivity.celsiusActivated = sharedPreferences.getBoolean("celsiusActivated", true);
            latitudes = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("latitudes", ObjectSerializer.serialize(new ArrayList<String>())));
            longitude = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("longitude", ObjectSerializer.serialize(new ArrayList<String>())));

            if (latitudes != null && longitude != null) {
                if (latitudes.size() > 0 && longitude.size() > 0) {
                    if (latitudes.size() == longitude.size()) {
                        for (int i = 0; i < latitudes.size(); i++) {
                            MainActivity.locations.add(new LatLng(Double.parseDouble(latitudes.get(i)), Double.parseDouble(longitude.get(i))));
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
