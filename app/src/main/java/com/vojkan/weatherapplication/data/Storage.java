package com.vojkan.weatherapplication.data;

public interface Storage {

    void saveData();

    void loadData();

}
