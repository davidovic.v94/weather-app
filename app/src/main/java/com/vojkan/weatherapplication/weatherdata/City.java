package com.vojkan.weatherapplication.weatherdata;

import lombok.Data;

@Data
public class City {

    private String cityName;
    private Coordinates coordinates;
    private Country country;
    private String postalCode;

}
