package com.vojkan.weatherapplication.weatherdata;

import org.json.JSONObject;

import lombok.Data;

@Data
public class Direction {

    private Double value;
    private String code;
    private String name;

    public Direction (JSONObject directionJSON) {
        try {
            this.value = directionJSON.getDouble("wind_deg");
            degreesToDirection(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void degreesToDirection(Double value) {
        if (value >= 0.0 && value <= 11.25) {
            this.code = "N";
            this.name = "North";
        } else if (value > 11.25 && value <= 33.75) {
            this.code = "NNE";
            this.name = "NorthNorthEast";
        } else if (value > 33.75 && value <= 56.25) {
            this.code = "NE";
            this.name = "NorthEast";
        } else if (value > 56.25 && value <= 78.75) {
            this.code = "ENE";
            this.name = "EastNorthEast";
        } else if (value > 78.75 && value <= 101.25) {
            this.code = "E";
            this.name = "East";
        } else if (value > 101.25 && value <= 123.75) {
            this.code = "ESE";
            this.name = "EastSouthEast";
        } else if (value > 123.75 && value <= 146.25) {
            this.code = "SE";
            this.name = "SouthEast";
        } else if (value > 146.25 && value <= 168.75) {
            this.code = "SSE";
            this.name = "SouthSouthEast";
        } else if (value > 168.75 && value <= 191.25) {
            this.code = "S";
            this.name = "South";
        } else if (value > 191.25 && value <= 213.75) {
            this.code = "SSW";
            this.name = "SouthSouthWest";
        } else if (value > 213.75 && value <= 236.25) {
            this.code = "SW";
            this.name = "SouthWest";
        } else if (value > 236.25 && value <= 258.75) {
            this.code = "WSW";
            this.name = "WestSouthWest";
        } else if (value > 258.75 && value <= 281.25) {
            this.code = "W";
            this.name = "West";
        } else if (value > 281.25 && value <= 303.75) {
            this.code = "WNW";
            this.name = "WestNorthWest";
        } else if (value > 303.75 && value <= 326.25) {
            this.code = "NW";
            this.name = "NorthWest";
        } else if (value > 326.25 && value <= 348.75) {
            this.code = "NNW";
            this.name = "NorthNorthWest";
        } else if (value > 348.75 && value <= 360.00) {
            this.code = "N";
            this.name = "North";
        }
    }

}
