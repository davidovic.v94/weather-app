package com.vojkan.weatherapplication.weatherdata;

import org.json.JSONObject;

import lombok.Data;

@Data
public class Pressure {

    private Double pressure;
    private Double seaLevel;
    private Double groundLevel;
    private String unit = "hPa";

    public Pressure (JSONObject pressureJSON) {
        try {
            this.pressure = pressureJSON.getDouble("pressure");
//            this.seaLevel = pressureJSON.getDouble("sea_level");
//            this.groundLevel = pressureJSON.getDouble("grnd_level");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
