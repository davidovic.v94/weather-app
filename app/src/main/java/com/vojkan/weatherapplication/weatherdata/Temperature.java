package com.vojkan.weatherapplication.weatherdata;

import org.json.JSONObject;

import lombok.Data;

@Data
public class Temperature {

    private Double current;
    private Double day;
    private Double night;
    private Double evening;
    private Double morning;
    private Double feelsLike;
    private Double min;
    private Double max;
    public static String unit = "℃";

    public Temperature (JSONObject temperatureJSON, JSONObject minMaxJSON) {
        try {
            this.current = temperatureJSON.getDouble("temp");
            this.feelsLike = temperatureJSON.getDouble("feels_like");
            this.min = minMaxJSON.getDouble("min");
            this.max = minMaxJSON.getDouble("max");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toFahrenheit() {
        this.current = celsiusToFahrenheit(this.current);
        this.day = celsiusToFahrenheit(this.day);
        this.night = celsiusToFahrenheit(this.night);
        this.evening = celsiusToFahrenheit(this.evening);
        this.morning = celsiusToFahrenheit(this.morning);
        this.feelsLike = celsiusToFahrenheit(this.feelsLike);
        this.min = celsiusToFahrenheit(this.min);
        this.max = celsiusToFahrenheit(this.max);
        unit = "℉";
    }

    public void toCelsius() {
        this.current = fahrenheitToCelsius(this.current);
        this.day = fahrenheitToCelsius(this.day);
        this.night = fahrenheitToCelsius(this.night);
        this.evening = fahrenheitToCelsius(this.evening);
        this.morning = fahrenheitToCelsius(this.morning);
        this.feelsLike = fahrenheitToCelsius(this.feelsLike);
        this.min = fahrenheitToCelsius(this.min);
        this.max = fahrenheitToCelsius(this.max);
        unit = "℃";
    }

    public Double celsiusToFahrenheit(Double celsius) {
        return celsius == null ?  null : celsius * 1.8 + 32;
    }

    public Double fahrenheitToCelsius(Double fahrenheit) {
        return fahrenheit == null ?  null : (fahrenheit - 32)/1.8;
    }

}
