package com.vojkan.weatherapplication.weatherdata;

import org.json.JSONObject;

import lombok.Data;

@Data
public class Precipitation {

    private Double value;
    private Double lastHour;
    private Double lastThreeHours;
    private String mode;
    private int pop;
    private String unit = "mm";

    public Precipitation(Double value, Double lastHour, Double lastThreeHours, String mode, Double pop) {
        this.value = value;
        this.lastHour = lastHour;
        this.lastThreeHours = lastThreeHours;
        this.mode = mode;
        this.pop = (int) (pop * 100);
    }

    public Precipitation (JSONObject precipitationJSON, String mode, Double pop) {
        try {
            this.lastHour = precipitationJSON.getDouble("1h");
//            this.lastThreeHours = precipitationJSON.getDouble("3h");
            this.mode = mode;
            this.pop = (int) (pop * 100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
