package com.vojkan.weatherapplication.weatherdata;

import org.json.JSONObject;

import lombok.Data;

@Data
public class Wind {

    private String name;
    private Speed speed;
    private Direction direction;
    private Double gust;

    public Wind (JSONObject windJSON) {
        try {
            this.speed = new Speed(windJSON);
            this.direction = new Direction(windJSON);
//            this.gust = windJSON.getDouble("gust");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
