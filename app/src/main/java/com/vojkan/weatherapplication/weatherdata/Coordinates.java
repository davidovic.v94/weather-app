package com.vojkan.weatherapplication.weatherdata;

import lombok.Data;

@Data
public class Coordinates {

    private Double longitude;
    private Double latitude;
    private Double altitude;

    public Coordinates(Double latitude, Double longitude, Double altitude) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.altitude = altitude;
    }

}
