package com.vojkan.weatherapplication.weatherdata;

import org.json.JSONObject;

import lombok.Data;

@Data
public class Speed {

    private Double value;
    private String unit = "m/s";

    public Speed (JSONObject speedJSON) {
        try {
            this.value = speedJSON.getDouble("wind_speed");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
