package com.vojkan.weatherapplication.weatherdata;

import org.json.JSONObject;

import lombok.Data;

@Data
public class WeatherDetails {

    private String value;
    private String description;
    private String icon;

    public WeatherDetails (JSONObject weatherDetailsJSON) {
        try {
            this.value = weatherDetailsJSON.getString("main");
            this.description = weatherDetailsJSON.getString("description");
            this.icon = weatherDetailsJSON.getString("icon");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
