package com.vojkan.weatherapplication.weatherdata;

import lombok.Data;

@Data
public class Country {

    private String countryId;
    private String countryName;

}
