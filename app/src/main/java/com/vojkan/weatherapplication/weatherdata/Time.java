package com.vojkan.weatherapplication.weatherdata;

import com.vojkan.weatherapplication.utils.DateConverter;

import org.json.JSONObject;

import java.util.Locale;

import lombok.Data;

@Data
public class Time {

    private Long timestamp;
    private Long timezoneOffsetMilliseconds;
    private String timeOfCalculation;
    private String timezoneOffset;
    private String timezoneID;
    private String sunrise;
    private String sunset;

    public Time(JSONObject sysJson, JSONObject weatherObject) {
        try {
            this.timestamp = sysJson.getLong("dt");
            this.timezoneOffsetMilliseconds = weatherObject.getLong("timezone_offset");
            this.timeOfCalculation = DateConverter.timestampToDate(sysJson.getLong("dt"), weatherObject.getLong("timezone_offset")).toString();
            int tz = DateConverter.getTimezone(weatherObject.getLong("timezone_offset"));
            if (tz < 0) {
                this.timezoneOffset = String.format(Locale.getDefault(), "%d", tz);
            } else if (tz > 0) {
                this.timezoneOffset = String.format(Locale.getDefault(), "+%d", tz);
            } else {
                this.timezoneOffset = String.format(Locale.getDefault(), "%d", 0);
            }
            this.timezoneID = weatherObject.getString("timezone");
            this.sunrise = DateConverter.timestampToDate(sysJson.getLong("sunrise"), weatherObject.getLong("timezone_offset")).toString();
            this.sunset = DateConverter.timestampToDate(sysJson.getLong("sunset"), weatherObject.getLong("timezone_offset")).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
