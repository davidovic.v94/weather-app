package com.vojkan.weatherapplication.weatherdata;

import org.json.JSONObject;

import lombok.Data;

@Data
public class Clouds {

    private Double value;
    private String name;

    public Clouds(JSONObject cloudsJSON) {
        try {
            this.value = cloudsJSON.getDouble("clouds");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
