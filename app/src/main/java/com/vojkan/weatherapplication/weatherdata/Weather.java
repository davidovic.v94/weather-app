package com.vojkan.weatherapplication.weatherdata;

import com.vojkan.weatherapplication.utils.JSONNullChecker;

import org.json.JSONObject;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Weather {

    private City city;
    private Temperature temperature;
    private Double humidity;
    private Pressure pressure;
    private Wind wind;
    private Clouds clouds;
    private Double visibility;
    private Double uvi;
    private Precipitation precipitation;
    private WeatherDetails weatherDetails;
    private Time time;

    public Weather() {

    }

    public Weather(JSONObject weatherJSON) {
        try {
            this.temperature = new Temperature(JSONNullChecker.checkForNullValueObject(weatherJSON, "current"),
                    JSONNullChecker.checkForNullValueObject(JSONNullChecker.checkForNullValueArray(weatherJSON, "daily").getJSONObject(0), "temp"));
            this.humidity = weatherJSON.getJSONObject("current").getDouble("humidity");
            this.pressure = new Pressure(JSONNullChecker.checkForNullValueObject(weatherJSON, "current"));
            this.wind = new Wind(JSONNullChecker.checkForNullValueObject(weatherJSON, "current"));
            this.clouds = new Clouds(JSONNullChecker.checkForNullValueObject(weatherJSON, "current"));
            this.uvi = weatherJSON.getJSONObject("current").getDouble("uvi");
            this.visibility = weatherJSON.getJSONObject("current").getDouble("visibility");
            if (JSONNullChecker.checkForNullValueObject(weatherJSON.getJSONObject("current"), "rain") != null) {
                this.precipitation = new Precipitation(new JSONObject(weatherJSON.getJSONObject("current").getString("rain")), "rain",
                        weatherJSON.getJSONArray("hourly").getJSONObject(0).getDouble("pop"));
            } else if (JSONNullChecker.checkForNullValueObject(weatherJSON.getJSONObject("current"), "snow") != null) {
                this.precipitation = new Precipitation(new JSONObject(weatherJSON.getJSONObject("current").getString("snow")), "snow",
                        weatherJSON.getJSONArray("hourly").getJSONObject(0).getDouble("pop"));
            } else {
                this.precipitation = new Precipitation(null, null, null, null,
                        weatherJSON.getJSONArray("hourly").getJSONObject(0).getDouble("pop"));
            }
            this.weatherDetails = new WeatherDetails(JSONNullChecker.checkForNullValueArray(weatherJSON.getJSONObject("current"), "weather").getJSONObject(0));
            this.time = new Time(JSONNullChecker.checkForNullValueObject(weatherJSON, "current"), weatherJSON);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
