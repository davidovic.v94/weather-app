package com.vojkan.weatherapplication.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextClock;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vojkan.weatherapplication.R;
import com.vojkan.weatherapplication.activities.MainActivity;
import com.vojkan.weatherapplication.utils.ImageManager;
import com.vojkan.weatherapplication.weatherdata.Weather;

import java.util.List;
import java.util.Locale;

public class WeatherListAdapter extends RecyclerView.Adapter<WeatherListAdapter.WeatherListViewHolder> {

    private final List<Weather> weatherList;
    private OnItemClickListener onItemClickListener;
    private final ImageManager imageManager = new ImageManager();

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public WeatherListAdapter(List<Weather> weatherList) {
        this.weatherList = weatherList;
    }

    public static class WeatherListViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout linearLayoutListItem;
        private final TextClock timeTextClock;
        private final TextView citynameTextView;
        private final TextView temperatureTextView;
        private final ImageView locationImageView;
        private final ImageView weatherIconImageView;

        public WeatherListViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            linearLayoutListItem = itemView.findViewById(R.id.list_item);
            timeTextClock = itemView.findViewById(R.id.list_item_time);
            citynameTextView = itemView.findViewById(R.id.list_item_city_name);
            temperatureTextView = itemView.findViewById(R.id.list_item_temperature);
            locationImageView = itemView.findViewById(R.id.list_item_location_icon);
            weatherIconImageView = itemView.findViewById(R.id.list_item_weather_icon);

            itemView.setOnClickListener(view -> {
                if (onItemClickListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onItemClickListener.onItemClick(position);
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public WeatherListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new WeatherListViewHolder(v, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherListViewHolder holder, int position) {
        Weather weather = weatherList.get(position);
        String iconCode = weather.getWeatherDetails().getIcon();
        String timezone = weather.getTime().getTimezoneID();
        String cityName = weather.getCity().getCityName();
        int temperature = weather.getTemperature().getCurrent().intValue();

        holder.linearLayoutListItem.setBackgroundResource(imageManager.setBackgroundColor(iconCode));
        holder.weatherIconImageView.setImageResource(imageManager.getWeatherIcon(iconCode));

        if (weather.getCity().getCityName().equals(MainActivity.locationFragmentTitle)) {
            holder.locationImageView.setImageResource(R.drawable.ic_baseline_near_me_24);
        }

        holder.timeTextClock.setTimeZone(timezone);
        holder.citynameTextView.setText(cityName);
        holder.temperatureTextView.setText(String.format(Locale.getDefault(), "%d°", temperature));
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

}
