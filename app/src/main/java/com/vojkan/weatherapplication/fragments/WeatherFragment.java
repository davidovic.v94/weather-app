package com.vojkan.weatherapplication.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.vojkan.weatherapplication.R;
import com.vojkan.weatherapplication.activities.MainActivity;
import com.vojkan.weatherapplication.utils.DateConverter;
import com.vojkan.weatherapplication.utils.ImageManager;
import com.vojkan.weatherapplication.utils.LocationDataManager;
import com.vojkan.weatherapplication.weatherdata.City;
import com.vojkan.weatherapplication.weatherdata.Weather;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

public class WeatherFragment extends Fragment {

    private LinearLayout fragmentLayout;
    private LinearLayout hourlyWeatherForecastLayout;
    private String newSunriseTime, newSunsetTime;
    private Long newSunriseTimestamp, newSunsetTimestamp;
    private String weatherInfo = null;
    private Weather weather;
    private String fragmentTitle;
    private ImageManager imageManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            setFragmentTitle(getArguments().getString("fragmentTitle"));
            weatherInfo = getArguments().getString("weatherInfo");
            try {
                if (weatherInfo != null) {
                    JSONObject jsonObject = new JSONObject(weatherInfo);
                    weather = new Weather(jsonObject);
                    LocationDataManager locationDataManager = new LocationDataManager(getContext());
                    City city = locationDataManager.getCityFromLatLon(jsonObject.getDouble("lat"), jsonObject.getDouble("lon"));
                    weather.setCity(city);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_holder, container, false);

        fragmentLayout = view.findViewById(R.id.fragment_layout);
        HorizontalScrollView horizontalScrollView = (HorizontalScrollView) inflater.inflate(R.layout.hourly_weather_forecast_item_holder, fragmentLayout, false);
        fragmentLayout.addView(horizontalScrollView);
        hourlyWeatherForecastLayout = horizontalScrollView.findViewById(R.id.hourly_weather_forecast_holder_test);

        imageManager = new ImageManager();

        checkSunsetSunriseTime(weatherInfo);
        getHourlyForecast(weatherInfo, inflater);
        getDailyForecast(weatherInfo, inflater);
        getCurrentWeatherInfo(inflater);
        getCurrentWeatherDetails(inflater);

        return view;
    }

    public static WeatherFragment newInstance(String weatherInfo, String fragmentTitle) {
        Bundle args = new Bundle();
        args.putString("weatherInfo", weatherInfo);
        args.putString("fragmentTitle", fragmentTitle);

        WeatherFragment fragment = new WeatherFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void getHourlyForecast(String string, LayoutInflater inflater) {
        try {
            JSONArray jsonArray = new JSONArray(new JSONObject(string).getString("hourly"));
            for (int i = 0; i < 25; i++) {
                View hourlyWeatherForecastItem = inflater.inflate(R.layout.hourly_weather_forecast_item, hourlyWeatherForecastLayout, false);
                TextView textViewTime = hourlyWeatherForecastItem.findViewById(R.id.hourly_weather_forecast_time_text_view);
                TextView textViewChanceOfRain = hourlyWeatherForecastItem.findViewById(R.id.hourly_weather_forecast_chance_of_rain_text_view);
                ImageView imageViewIcon = hourlyWeatherForecastItem.findViewById(R.id.hourly_weather_forecast_icon);
                TextView textViewTemperature = hourlyWeatherForecastItem.findViewById(R.id.hourly_weather_forecast_temperature_text_view);
                if (i == 0) {
                    textViewTime.setText(R.string.hourly_weather_forecaast_time_now);
                } else {
                    textViewTime.setText(String.format(Locale.getDefault(), "%s",
                            DateConverter.getHourFromDate(DateConverter.timestampToDate(jsonArray.getJSONObject(i).getLong("dt"),
                                    new JSONObject(string).getLong("timezone_offset")))));
                }
                if (jsonArray.getJSONObject(i).getDouble("pop") * 100 > 20) {
                    double popDouble = jsonArray.getJSONObject(i).getDouble("pop");
                    int popInteger = (int) (popDouble * 100);
                    String popString = String.format(Locale.getDefault(), "%d%%", popInteger);
                    textViewChanceOfRain.setText(popString);
                }
                imageViewIcon.setImageResource(imageManager.getWeatherIcon(jsonArray.getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("icon")));
                if (!MainActivity.celsiusActivated) {
                    textViewTemperature.setText(String.format(Locale.getDefault(), "%d%s", (int) (jsonArray.getJSONObject(i).getDouble("temp") * 1.8 + 32),
                            getResources().getString(R.string.temperature_char_degree)));
                } else {
                    textViewTemperature.setText(String.format(Locale.getDefault(), "%d%s", jsonArray.getJSONObject(i).getInt("temp"),
                            getResources().getString(R.string.temperature_char_degree)));
                }
                hourlyWeatherForecastLayout.addView(hourlyWeatherForecastItem);
                if (newSunriseTime != null) {
                    if (DateConverter.compareTimeInHour(jsonArray.getJSONObject(i).getLong("dt"),
                            newSunriseTimestamp, new JSONObject(string).getLong("timezone_offset"))) {
                        setSunriseTime(inflater);
                    }
                } else {
                    if (DateConverter.compareTimeInHour(jsonArray.getJSONObject(i).getLong("dt"),
                            new JSONObject(string).getJSONObject("current").getLong("sunrise"), new JSONObject(string).getLong("timezone_offset"))) {
                        setSunriseTime(inflater);
                    }
                }
                if (newSunsetTime != null) {
                    if (DateConverter.compareTimeInHour(jsonArray.getJSONObject(i).getLong("dt"),
                            newSunsetTimestamp, new JSONObject(string).getLong("timezone_offset"))) {//
                        setSunsetTime(inflater);
                    }
                } else {
                    if (DateConverter.compareTimeInHour(jsonArray.getJSONObject(i).getLong("dt"),
                            new JSONObject(string).getJSONObject("current").getLong("sunset"), new JSONObject(string).getLong("timezone_offset"))) {//
                        setSunsetTime(inflater);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getDailyForecast(String string, LayoutInflater inflater) {
        try {
            JSONArray jsonArray = new JSONArray(new JSONObject(string).getString("daily"));
            for (int i = 1; i < jsonArray.length(); i++) {
                View view = inflater.inflate(R.layout.weekly_weather_forecast_item, fragmentLayout, false);
                TextView textViewDay = view.findViewById(R.id.weekly_weather_forecast_item_day);
                ImageView imageViewIcon = view.findViewById(R.id.weekly_weather_forecast_item_icon);
                TextView textViewTemperatureMax = view.findViewById(R.id.temperature_range_max_temperature);
                TextView textViewTemperatureMin = view.findViewById(R.id.temperature_range_min_temperature);
                textViewDay.setText(DateConverter.getDayFromDate(DateConverter.timestampToDate(jsonArray.getJSONObject(i).getLong("dt"),
                        new JSONObject(string).getLong("timezone_offset"))));
                imageViewIcon.setImageResource(imageManager.getWeatherIcon(jsonArray.getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("icon")));
                if (!MainActivity.celsiusActivated) {
                    textViewTemperatureMax.setText(String.format(Locale.getDefault(), "%d%s", (int) (jsonArray.getJSONObject(i).getJSONObject("temp").getDouble("max") * 1.8 + 32),
                            getResources().getString(R.string.temperature_char_degree)));
                    textViewTemperatureMin.setText(String.format(Locale.getDefault(), "%d%s", (int) (jsonArray.getJSONObject(i).getJSONObject("temp").getInt("min") * 1.8 + 32),
                            getResources().getString(R.string.temperature_char_degree)));
                } else {
                    textViewTemperatureMax.setText(String.format(Locale.getDefault(), "%d%s", jsonArray.getJSONObject(i).getJSONObject("temp").getInt("max"),
                            getResources().getString(R.string.temperature_char_degree)));
                    textViewTemperatureMin.setText(String.format(Locale.getDefault(), "%d%s", jsonArray.getJSONObject(i).getJSONObject("temp").getInt("min"),
                            getResources().getString(R.string.temperature_char_degree)));
                }
                fragmentLayout.addView(view);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCurrentWeatherInfo(LayoutInflater inflater) {
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.current_weather_details_info, fragmentLayout, false);
        TextView textView = (TextView) linearLayout.getChildAt(0);
        textView.setText(String.format(Locale.getDefault(), "Today: %s; It's %d° currently; the high today was forecast as %d°.",
                weather.getWeatherDetails().getDescription(), weather.getTemperature().getCurrent().intValue(), weather.getTemperature().getMax().intValue()));
        fragmentLayout.addView(linearLayout);
    }

    private void getCurrentWeatherDetails(LayoutInflater inflater) {
        TableLayout tableLayout = (TableLayout) inflater.inflate(R.layout.current_weather_details_layout, fragmentLayout, false);
        for (int i = 0; i < tableLayout.getChildCount(); i++) {
            TableRow tableRow = (TableRow) tableLayout.getChildAt(i);
            for (int j = 0; j < tableRow.getChildCount(); j++) {
                LinearLayout currentWeatherDetailsItem = (LinearLayout) tableRow.getChildAt(j);
                final int childCount = currentWeatherDetailsItem.getChildCount();
                switch (currentWeatherDetailsItem.getId()) {
                    case R.id.current_weather_details_sunrise:
                        for (int k = 0; k < childCount; k++) {
                            View v = currentWeatherDetailsItem.getChildAt(k);
                            TextView textView = (TextView) v;
                            if (v == currentWeatherDetailsItem.findViewById(R.id.weather_info_item_header)) {
                                textView.setText(getResources().getString(R.string.sunrise));
                            } else {
                                textView.setText(DateConverter.getTime(weather.getTime().getSunrise()));
                            }
                        }
                        break;
                    case R.id.current_weather_details_sunset:
                        for (int k = 0; k < childCount; k++) {
                            View v = currentWeatherDetailsItem.getChildAt(k);
                            TextView textView = (TextView) v;
                            if (v == currentWeatherDetailsItem.findViewById(R.id.weather_info_item_header)) {
                                textView.setText(getResources().getString(R.string.sunset));
                            } else {
                                textView.setText(DateConverter.getTime(weather.getTime().getSunset()));
                            }
                        }
                        break;
                    case R.id.current_weather_details_chance_of_rain:
                        for (int k = 0; k < childCount; k++) {
                            View v = currentWeatherDetailsItem.getChildAt(k);
                            TextView textView = (TextView) v;
                            if (v == currentWeatherDetailsItem.findViewById(R.id.weather_info_item_header)) {
                                textView.setText(getResources().getString(R.string.chance_of_rain));
                            } else {
                                textView.setText(String.format(Locale.getDefault(), "%d%%", weather.getPrecipitation().getPop()));
                            }
                        }
                        break;
                    case R.id.current_weather_details_humidity:
                        for (int k = 0; k < childCount; k++) {
                            View v = currentWeatherDetailsItem.getChildAt(k);
                            TextView textView = (TextView) v;
                            if (v == currentWeatherDetailsItem.findViewById(R.id.weather_info_item_header)) {
                                textView.setText(getResources().getString(R.string.humidity));
                            } else {
                                textView.setText(String.format(Locale.getDefault(), "%d%%", weather.getHumidity().intValue()));
                            }
                        }
                        break;
                    case R.id.current_weather_details_wind:
                        for (int k = 0; k < childCount; k++) {
                            View v = currentWeatherDetailsItem.getChildAt(k);
                            TextView textView = (TextView) v;
                            if (v == currentWeatherDetailsItem.findViewById(R.id.weather_info_item_header)) {
                                textView.setText(getResources().getString(R.string.wind));
                            } else {
                                textView.setText(String.format(Locale.getDefault(), "%s %.1f %s", weather.getWind().getDirection().getCode(),
                                        weather.getWind().getSpeed().getValue(), weather.getWind().getSpeed().getUnit()));
                            }
                        }
                        break;
                    case R.id.current_weather_details_feels_like:
                        for (int k = 0; k < childCount; k++) {
                            View v = currentWeatherDetailsItem.getChildAt(k);
                            TextView textView = (TextView) v;
                            if (v == currentWeatherDetailsItem.findViewById(R.id.weather_info_item_header)) {
                                textView.setText(getResources().getString(R.string.feels_like));
                            } else {
                                textView.setText(String.format(Locale.getDefault(), "%d%s", weather.getTemperature().getFeelsLike().intValue(),
                                        getResources().getString(R.string.temperature_char_degree)));
                            }
                        }
                        break;
                    case R.id.current_weather_details_precipitation:
                        for (int k = 0; k < childCount; k++) {
                            View v = currentWeatherDetailsItem.getChildAt(k);
                            TextView textView = (TextView) v;
                            if (v == currentWeatherDetailsItem.findViewById(R.id.weather_info_item_header)) {
                                textView.setText(getResources().getString(R.string.precipitation));
                            } else {
                                if (weather.getPrecipitation().getLastHour() == null) {
                                    textView.setText(String.format(Locale.getDefault(), "%d %s", 0,
                                            weather.getPrecipitation().getUnit()));
                                } else {
                                    textView.setText(String.format(Locale.getDefault(), "%.2f %s", weather.getPrecipitation().getLastHour(),
                                            weather.getPrecipitation().getUnit()));
                                }
                            }
                        }
                        break;
                    case R.id.current_weather_details_pressure:
                        for (int k = 0; k < childCount; k++) {
                            View v = currentWeatherDetailsItem.getChildAt(k);
                            TextView textView = (TextView) v;
                            if (v == currentWeatherDetailsItem.findViewById(R.id.weather_info_item_header)) {
                                textView.setText(getResources().getString(R.string.pressure));
                            } else {
                                textView.setText(String.format(Locale.getDefault(), "%d %s", weather.getPressure().getPressure().intValue(),
                                        weather.getPressure().getUnit()));
                            }
                        }
                        break;
                    case R.id.current_weather_details_visibility:
                        for (int k = 0; k < childCount; k++) {
                            View v = currentWeatherDetailsItem.getChildAt(k);
                            TextView textView = (TextView) v;
                            if (v == currentWeatherDetailsItem.findViewById(R.id.weather_info_item_header)) {
                                textView.setText(getResources().getString(R.string.visibility));
                            } else {
                                if (weather.getVisibility() > 1000) {
                                    textView.setText(String.format(Locale.getDefault(), "%.1f km", weather.getVisibility() / 1000));
                                } else {
                                    textView.setText(String.format(Locale.getDefault(), "%d m", weather.getVisibility().intValue()));
                                }
                            }
                        }
                        break;
                    case R.id.current_weather_details_uv_index:
                        for (int k = 0; k < childCount; k++) {
                            View v = currentWeatherDetailsItem.getChildAt(k);
                            TextView textView = (TextView) v;
                            if (v == currentWeatherDetailsItem.findViewById(R.id.weather_info_item_header)) {
                                textView.setText(getResources().getString(R.string.uv_index));
                            } else {
                                textView.setText(String.format(Locale.getDefault(), "%d", weather.getUvi().intValue()));
                            }
                        }
                        break;
                }
            }
        }
        fragmentLayout.addView(tableLayout);
    }

    private void checkSunsetSunriseTime(String string) {
        try {
            JSONObject jsonObject = new JSONObject(string);
            if (DateConverter.compareTime(jsonObject.getJSONArray("daily").getJSONObject(0).getLong("sunrise"),
                    jsonObject.getJSONObject("current").getLong("dt"), new JSONObject(string).getLong("timezone_offset"))) {
                newSunriseTime = DateConverter.timestampToDate(jsonObject.getJSONArray("daily").getJSONObject(1).getLong("sunrise"),
                        new JSONObject(string).getLong("timezone_offset")).toString();
                newSunriseTimestamp = jsonObject.getJSONArray("daily").getJSONObject(1).getLong("sunrise");
            }
            if (DateConverter.compareTime(jsonObject.getJSONArray("daily").getJSONObject(0).getLong("sunset"),
                    jsonObject.getJSONObject("current").getLong("dt"), new JSONObject(string).getLong("timezone_offset"))) {
                newSunsetTime = DateConverter.timestampToDate(jsonObject.getJSONArray("daily").getJSONObject(1).getLong("sunset"),
                        new JSONObject(string).getLong("timezone_offset")).toString();
                newSunsetTimestamp = jsonObject.getJSONArray("daily").getJSONObject(1).getLong("sunset");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSunriseTime(LayoutInflater inflater) {
        View hourlyWeatherForecastItem = inflater.inflate(R.layout.hourly_weather_forecast_item, hourlyWeatherForecastLayout, false);
        TextView textViewTime = hourlyWeatherForecastItem.findViewById(R.id.hourly_weather_forecast_time_text_view);
        ImageView imageViewIcon = hourlyWeatherForecastItem.findViewById(R.id.hourly_weather_forecast_icon);
        TextView textViewTemperature = hourlyWeatherForecastItem.findViewById(R.id.hourly_weather_forecast_temperature_text_view);
        if (newSunriseTime == null) {
            textViewTime.setText(String.format(Locale.getDefault(), "%s",
                    DateConverter.getTime(weather.getTime().getSunrise())));
        } else {
            textViewTime.setText(String.format(Locale.getDefault(), "%s",
                    DateConverter.getTime(newSunriseTime)));
        }
        imageViewIcon.setImageResource(R.drawable.sunrise);
        textViewTemperature.setText(getResources().getString(R.string.sunrise));
        hourlyWeatherForecastLayout.addView(hourlyWeatherForecastItem);
    }

    private void setSunsetTime(LayoutInflater inflater) {
        View hourlyWeatherForecastItem = inflater.inflate(R.layout.hourly_weather_forecast_item, hourlyWeatherForecastLayout, false);
        TextView textViewTime = hourlyWeatherForecastItem.findViewById(R.id.hourly_weather_forecast_time_text_view);
        ImageView imageViewIcon = hourlyWeatherForecastItem.findViewById(R.id.hourly_weather_forecast_icon);
        TextView textViewTemperature = hourlyWeatherForecastItem.findViewById(R.id.hourly_weather_forecast_temperature_text_view);
        if (newSunsetTime == null) {
            textViewTime.setText(String.format(Locale.getDefault(), "%s",
                    DateConverter.getTime(weather.getTime().getSunset())));
        } else {
            textViewTime.setText(String.format(Locale.getDefault(), "%s",
                    DateConverter.getTime(newSunsetTime)));
        }
        imageViewIcon.setImageResource(R.drawable.sunset);
        textViewTemperature.setText(getResources().getString(R.string.sunset));
        hourlyWeatherForecastLayout.addView(hourlyWeatherForecastItem);
    }

    public String getFragmentTitle() {
        return fragmentTitle;
    }

    public void setFragmentTitle(String fragmentTitle) {
        this.fragmentTitle = fragmentTitle;
    }
}