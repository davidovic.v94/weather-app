package com.vojkan.weatherapplication.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.textfield.TextInputLayout;
import com.vojkan.weatherapplication.R;
import com.vojkan.weatherapplication.activities.MainActivity;
import com.vojkan.weatherapplication.activities.PlacesActivity;
import com.vojkan.weatherapplication.data.DataManage;
import com.vojkan.weatherapplication.network.BaseTask;
import com.vojkan.weatherapplication.network.TaskRunner;
import com.vojkan.weatherapplication.utils.LocationDataManager;
import com.vojkan.weatherapplication.weatherdata.City;
import com.vojkan.weatherapplication.weatherdata.Weather;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class SearchDialog extends DialogFragment implements View.OnKeyListener {

    private TextInputLayout searchEditText;
    private String cityName;
    private final TaskRunner taskRunner = new TaskRunner();
    private final List<City> searchResultCityList = new ArrayList<>();
    private final List<String> searchResultCityNameList = new ArrayList<>();
    private ListView listView;
    private ArrayAdapter<String> listAdapter;
    private int cityIndex;
    private DataManage dataManage;
    private LocationDataManager locationDataManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullscreenDialogTheme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_dialog, container, false);

        searchEditText = view.findViewById(R.id.search_edit_text);
        Button searchButton = view.findViewById(R.id.search_button);
        Button cancelButton = view.findViewById(R.id.cancel_button);
        listView = view.findViewById(R.id.list_view_cities);
        dataManage = new DataManage(MainActivity.sharedPreferences);
        locationDataManager = new LocationDataManager(getContext());

        searchButton.setOnClickListener(view1 -> search());
        cancelButton.setOnClickListener(view12 -> dismiss());
        listView.setOnItemClickListener(onItemClickListener);

        return view;
    }

    AdapterView.OnItemClickListener onItemClickListener = (adapterView, view, i, l) -> {
        cityIndex = i;
        if (!MainActivity.places.contains(searchResultCityList.get(i))) {
            final String URL_LON_LAT_METRIC_TEST = "https://api.openweathermap.org/data/2.5/onecall?lat=" + searchResultCityList.get(i).getCoordinates().getLatitude() +
                    "&lon=" + searchResultCityList.get(i).getCoordinates().getLongitude() + "&exclude=minutely&appid=" + MainActivity.API_KEY + "&units=metric";
            taskRunner.executeAsync(new Task(URL_LON_LAT_METRIC_TEST));
        } else {
            Intent intent = new Intent(getContext(), PlacesActivity.class);
            startActivity(intent);
        }
    };

    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            search();
        }
        return false;
    }

    private void search() {
        try {
            if (searchResultCityList.size() != 0) {
                searchResultCityList.clear();
            }
            if (searchResultCityNameList.size() != 0) {
                searchResultCityNameList.clear();
            }
            if (getActivity() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
            }
            if (searchEditText != null && searchEditText.getEditText() != null) {
                cityName = searchEditText.getEditText().getText().toString();
            }
            String encodedCityName = URLEncoder.encode(cityName, "UTF-8");
            final String url = "http://geodb-free-service.wirefreethought.com/v1/geo/cities?limit=5&offset=0&namePrefix=" + encodedCityName;
            taskRunner.executeAsync(new Task(url));
        } catch (Exception e) {
            Toast.makeText(getContext(), "Could not find weather", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private class Task extends BaseTask<String> {
        String stringUrl;

        Task(final String stringUrl) {
            this.stringUrl = stringUrl;
        }

        @Override
        public void setUiForLoading() {
            super.setUiForLoading();
        }

        @Override
        public String call() {
            HttpURLConnection connection = null;
            URL url;

            try {
                url = new URL(stringUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-type", "application/json");

                InputStream in = connection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);

                int dataInputStream = reader.read();
                StringBuilder stringBuilder = new StringBuilder();

                while (dataInputStream != -1) {
                    char current = (char) dataInputStream;
                    stringBuilder.append(current);
                    dataInputStream = reader.read();
                }

                return stringBuilder.toString();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }

        @Override
        public void setDataAfterLoading(String result) {
            try {
                if (stringUrl.contains("geodb-free-service.wirefreethought.com")) {
                    Log.i("result", result);
                    getCityList(result);
                    if(searchResultCityList.size() == 0) {
                        Toast.makeText(getContext(), "Could not find city", Toast.LENGTH_SHORT).show();
                    } else {
                        if (getContext() != null) {
//                              listAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1,
//                                    searchResultCityList.stream().map(city -> city.getCityName() + ", " + city.getCountry().getCountryName()).collect(Collectors.toList()));
                                listAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, searchResultCityNameList);
                        }
                        listView.setAdapter(listAdapter);
                    }
                } else {
                    JSONObject jsonObject = new JSONObject(result);
                    Weather weather = new Weather(jsonObject);
                    weather.setCity(searchResultCityList.get(cityIndex));

                    if (!MainActivity.celsiusActivated) {
                        weather.getTemperature().toFahrenheit();
                    }

                    MainActivity.weatherList.add(weather);

                    PlacesActivity.adapter.notifyDataSetChanged();
                    addFragment(weather, result);

                    LatLng latLng = new LatLng(searchResultCityList.get(cityIndex).getCoordinates().getLatitude(), searchResultCityList.get(cityIndex).getCoordinates().getLongitude());
                    MainActivity.locations.add(latLng);
                    dataManage.saveData();

                    Log.i("Weather", weather.toString());
                    Intent intent = new Intent(getContext(), PlacesActivity.class);
                    startActivity(intent);
                }
            } catch (Exception e) {
                Toast.makeText(getContext(), "Could not find weather", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

    }

    private void getCityList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = new JSONArray(jsonObject.getString("data"));
            JSONObject dataJSON;
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < jsonArray.length(); i++) {
                dataJSON = jsonArray.getJSONObject(i);
                stringBuilder.append(dataJSON.getString("city")).append(", ").append(dataJSON.get("country"));
                searchResultCityNameList.add(stringBuilder.toString());
                stringBuilder.delete(0, stringBuilder.length());
                City city = locationDataManager.getCityFromLatLon(dataJSON.getDouble("latitude"), dataJSON.getDouble("longitude"));
                searchResultCityList.add(city);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addFragment(Weather weather, String result) {
        WeatherFragment weatherFragment = WeatherFragment.newInstance(result, weather.getCity().getCityName());
        MainActivity.viewPagerAdapter.addFragment(weatherFragment, weather.getCity().getCityName());
        MainActivity.viewPagerAdapter.notifyDataSetChanged();
    }

}
