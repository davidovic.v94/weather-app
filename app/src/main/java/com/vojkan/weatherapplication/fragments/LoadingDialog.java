package com.vojkan.weatherapplication.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.vojkan.weatherapplication.R;

public class LoadingDialog {

    private final Activity activity;
    private AlertDialog alertDialog;

    public LoadingDialog(Activity activity) {
        this.activity = activity;
    }

    @SuppressLint("InflateParams")
    public void startDialog() {
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(activity);
        materialAlertDialogBuilder.setView(activity.getLayoutInflater().inflate(R.layout.loading_dialog, null));
        materialAlertDialogBuilder.setBackground(new ColorDrawable(66000000));

        alertDialog = materialAlertDialogBuilder.create();
        alertDialog.show();
    }

    public void dismissDialog() {
        alertDialog.dismiss();
    }

    public boolean isShowing() {
        return alertDialog.isShowing();
    }

}
