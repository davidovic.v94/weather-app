package com.vojkan.weatherapplication.utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONNullChecker {

    public static JSONObject checkForNullValueObject(JSONObject jsonObject, String key) {
        try {
            if (!jsonObject.optString(key, "noMatch").equals("noMatch")) {
                return new JSONObject(jsonObject.getString(key));
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONArray checkForNullValueArray(JSONObject jsonObject, String key) {
        try {
            if (!jsonObject.optString(key, "noMatch").equals("noMatch")) {
                return new JSONArray(jsonObject.getString(key));
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
