package com.vojkan.weatherapplication.utils;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Locale;

public class DateConverter {

    public static OffsetDateTime timestampToDate(Long timestamp, Long timezone) {
        Instant instant = Instant.ofEpochMilli(timestamp * 1000);
        ZoneOffset zoneOffset = ZoneOffset.ofHours(getTimezone(timezone));
        return OffsetDateTime.ofInstant(instant, zoneOffset);
    }

    public static int getTimezone(Long seconds) {
        return (int) (seconds / 3600);
    }

    public static String getDayFromDate(OffsetDateTime date) {
        String dayOfWeek = date.getDayOfWeek().toString().toLowerCase();
        return dayOfWeek.substring(0, 1).toUpperCase() + dayOfWeek.substring(1);
    }

    public static String getHourFromDate(OffsetDateTime date) {
        if (date.getHour() < 10) {
            return String.format(Locale.getDefault(), "0%d", date.getHour());
        } else {
            return String.valueOf(date.getHour());
        }
    }

    public static String getMinuteFromDate(OffsetDateTime date) {
        if (date.getMinute() < 10) {
            return String.format(Locale.getDefault(), "0%d", date.getMinute());
        } else {
            return String.valueOf(date.getMinute());
        }
    }

    public static String getTime(String date) {
        OffsetDateTime offsetDateTime = OffsetDateTime.parse(date);
        String hour = getHourFromDate(offsetDateTime);
        String minute = getMinuteFromDate(offsetDateTime);
        return String.format(Locale.getDefault(), "%s:%s", hour, minute);
    }

    public static boolean compareTimeInHour(Long timestamp1, Long timestamp2, Long timezone) {
        OffsetDateTime date1 = timestampToDate(timestamp1, timezone);
        OffsetDateTime date2 = timestampToDate(timestamp2, timezone);
        if (date1.getHour() == date2.getHour()) {
            return date1.getMinute() < date2.getMinute();
        }
        return false;
    }

    public static boolean compareTime(Long timestamp1, Long timestamp2, Long timezone) {
        OffsetDateTime date1 = timestampToDate(timestamp1, timezone);
        OffsetDateTime date2 = timestampToDate(timestamp2, timezone);
        return date1.isBefore(date2);
    }

    public static boolean checkTimeIntervalBetweenRequests(String time, Long timezoneOffset) {
        OffsetDateTime date1 = OffsetDateTime.parse(time);
        Instant instant = Instant.now();
        ZoneOffset zoneOffset = ZoneOffset.ofHours(getTimezone(timezoneOffset));
        OffsetDateTime date2 = OffsetDateTime.ofInstant(instant, zoneOffset);
        return date1.getMinute() == date2.getMinute();
    }

}
