package com.vojkan.weatherapplication.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.vojkan.weatherapplication.weatherdata.City;
import com.vojkan.weatherapplication.weatherdata.Coordinates;
import com.vojkan.weatherapplication.weatherdata.Country;
import com.vojkan.weatherapplication.weatherdata.Weather;

import java.util.List;
import java.util.Locale;

public class LocationDataManager {

    private final Context context;

    public LocationDataManager(Context context) {
        this.context = context;
    }

    public City getCityFromLatLon(Double latitude, Double longitude) {
        Coordinates coordinates = new Coordinates(latitude, longitude, null);
        Country country = new Country();
        City city = new City();

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());

        try {
            List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
            if (addressList != null && addressList.size() > 0) {
                if (addressList.get(0).getCountryCode() != null) {
                    country.setCountryId(addressList.get(0).getCountryCode());
                }
                if (addressList.get(0).getCountryName() != null) {
                    country.setCountryName(addressList.get(0).getCountryName());
                }
                if (addressList.get(0).getLocality() != null) {
                    city.setCityName(addressList.get(0).getLocality());
                }
                if (city.getCityName() == null) {
                    city.setCityName(addressList.get(0).getSubAdminArea());
                }
                if (addressList.get(0).getPostalCode() != null) {
                    city.setPostalCode(addressList.get(0).getPostalCode());
                }
            }

            city.setCoordinates(coordinates);
            city.setCountry(country);

            return city;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void getCitiesFromLocations(List<City> cities, List<LatLng> locations) {
        if (locations.size() > 0) {
            for (int i = 0; i < locations.size(); i++) {
                cities.add(getCityFromLatLon(locations.get(i).latitude, locations.get(i).longitude));
            }
        }
    }

    public void removeLocation(City deletedCity, List<LatLng> locations) {
        for (LatLng latLng : locations) {
            if (latLng.latitude == deletedCity.getCoordinates().getLatitude() &&
                    latLng.longitude == deletedCity.getCoordinates().getLongitude()) {
                locations.remove(latLng);
                break;
            }
        }
    }

    public void removeCity(City deletedCity, List<City> places) {
        for (City city : places) {
            if (city.getCoordinates().getLatitude().equals(deletedCity.getCoordinates().getLatitude()) &&
                    city.getCoordinates().getLongitude().equals(deletedCity.getCoordinates().getLongitude())) {
                places.remove(city);
                break;
            }
        }
    }

    public boolean isNewLocation(Location location, List<LatLng> locations) {
        for (int i = 0; i < locations.size(); i++) {
            if (locations.get(i).longitude == location.getLongitude() && locations.get(i).latitude == location.getLatitude()) {
                return false;
            }
        }
        return true;
    }

    public void removeLocationFromLists(List<City> places, List<LatLng> locations, List<Weather> weatherList, List<String> weatherStringList, String locationFragmentTitle) {
        for (int i = 0; i < places.size(); i++) {
            if (places.get(i).getCityName().equals(locationFragmentTitle)) {
                locations.remove(i);
                places.remove(i);
                if (weatherList != null && weatherStringList != null) {
                    if (weatherList.size() > 0 && weatherStringList.size() > 0) {
                        weatherList.set(0, null);
                        weatherStringList.set(0, null);
                    }
                }
                break;
            }
        }
    }

}
