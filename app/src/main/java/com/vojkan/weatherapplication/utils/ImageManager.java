package com.vojkan.weatherapplication.utils;

import com.vojkan.weatherapplication.R;

public class ImageManager {

    public int setBackground(String code) {
        int retVal = 0;
        switch (code) {
            case "01d":
                retVal = R.drawable.clear_day_bg;
                break;
            case "01n":
                retVal = R.drawable.clear_night_bg;
                break;
            case "02d":
                retVal = R.drawable.few_clouds_day_bg;
                break;
            case "02n":
                retVal = R.drawable.few_clouds_night_bg;
                break;
            case "03d":
                retVal = R.drawable.scattered_clouds_day_bg;
                break;
            case "03n":
                retVal = R.drawable.scattered_clouds_night_bg;
                break;
            case "04d":
                retVal = R.drawable.broken_clouds_day_bg;
                break;
            case "04n":
                retVal = R.drawable.broken_clouds_night_bg;
                break;
            case "09d":
                retVal = R.drawable.shower_rain_day_bg;
                break;
            case "09n":
                retVal = R.drawable.shower_rain_night_bg;
                break;
            case "10d":
                retVal = R.drawable.rainy_day_bg;
                break;
            case "10n":
                retVal = R.drawable.rainy_night_bg;
                break;
            case "11d":
                retVal = R.drawable.thunderstorm_day_bg;
                break;
            case "11n":
                retVal = R.drawable.thunderstorm_night_bg;
                break;
            case "13d":
                retVal = R.drawable.snow_day_bg;
                break;
            case "13n":
                retVal = R.drawable.snow_night_bg;
                break;
            case "50d":
                retVal = R.drawable.mist_day_bg;
                break;
            case "50n":
                retVal = R.drawable.mist_night_bg;
                break;
        }
        return retVal;
    }

    public int setBackgroundColor(String code) {
        int retVal = 0;
        switch (code) {
            case "01d":
                retVal = R.color.clearDay;
                break;
            case "01n":
                retVal = R.color.clearNight;
                break;
            case "02d":
                retVal = R.color.fewCloudsDay;
                break;
            case "02n":
                retVal = R.color.fewCloudsNight;
                break;
            case "03d":
                retVal = R.color.scatteredCloudsDay;
                break;
            case "03n":
                retVal = R.color.scatteredCloudsNight;
                break;
            case "04d":
                retVal = R.color.blokenCloudsDay;
                break;
            case "04n":
                retVal = R.color.blokenCloudsNight;
                break;
            case "09d":
                retVal = R.color.showerRainDay;
                break;
            case "09n":
                retVal = R.color.showerRainNight;
                break;
            case "10d":
                retVal = R.color.rainyDay;
                break;
            case "10n":
                retVal = R.color.rainyNight;
                break;
            case "11d":
                retVal = R.color.thunderstormDay;
                break;
            case "11n":
                retVal = R.color.thunderstormNight;
                break;
            case "13d":
                retVal = R.color.snowDay;
                break;
            case "13n":
                retVal = R.color.snowNight;
                break;
            case "50d":
                retVal = R.color.mistDay;
                break;
            case "50n":
                retVal = R.color.mistNight;
                break;
        }
        return retVal;
    }

    public int getWeatherIcon(String code) {
        int retVal = 0;
        switch (code) {
            case "01d":
                retVal = R.drawable.clear_sky;
                break;
            case "02d":
                retVal = R.drawable.few_clouds_day;
                break;
            case "03d":
            case "03n":
                retVal = R.drawable.scattered_clouds;
                break;
            case "04d":
            case "04n":
                retVal = R.drawable.broken_clouds;
                break;
            case "09d":
            case "09n":
                retVal = R.drawable.shower_rain;
                break;
            case "10d":
                retVal = R.drawable.rain_day;
                break;
            case "11d":
            case "11n":
                retVal = R.drawable.thunderstorm;
                break;
            case "13d":
            case "13n":
                retVal = R.drawable.snow;
                break;
            case "50d":
            case "50n":
                retVal = R.drawable.mist;
                break;
            case "01n":
                retVal = R.drawable.clear_sky_night;
                break;
            case "02n":
                retVal = R.drawable.few_clouds_night;
                break;
            case "10n":
                retVal = R.drawable.rain_night;
                break;
        }
        return retVal;
    }

}
