# WeatherApp
> Android application for weather forcast.

## Table of contents
* [General info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Setup](#setup)
* [Status](#status)
* [Inspiration](#inspiration)

## General info
WeatherApp is real time app which uses device location (user must give permission) and Open Weather API. Open Weather API is free weather API where you can get various data collections (Current & Forecast weather data collection, Historical weather data collection, Maps collection and others). 

Application have different API calls for different purposes. It has info about current weather, hourly weather (sunrise and sunset, chance of rain), 7 days weather forecast and curent weather deatils (chance of rain, humidity, wind info, pressure, visibility and other). 

Also, you can search and add new places. New places are added into component called ViewPager that allows the user to flip left and right through pages of data. 

App contains the list of places with current location on top. Places can be removed and restored. 

It is possible to change the temperature unit (°C/°F).

## Screenshots

<img src="./images/Screenshot_1605699909.png" width="100">
<img src="./images/Screenshot_1605699941.png" width="100">
<img src="./images/Screenshot_1605699945.png" width="100">
<img src="./images/Screenshot_1605699951.png" width="100">
<img src="./images/Screenshot_1605699959.png" width="100">
<img src="./images/Screenshot_1605699966.png" width="100">
<img src="./images/Screenshot_1605699984.png" width="100">
<img src="./images/Screenshot_1605700009.png" width="100">
<img src="./images/Screenshot_1605700013.png" width="100">
<img src="./images/Screenshot_20210120_135411.png" width="100">
<img src="./images/Screenshot_20210120_135433.png" width="100">
<img src="./images/Screenshot_20210120_135441.png" width="100">
<img src="./images/Screenshot_20210120_135646.png" width="100">
<img src="./images/Screenshot_20210120_135655.png" width="100">
<img src="./images/Screenshot_20210120_135826.png" width="100">
<img src="./images/Screenshot_20210120_135845.png" width="100">

## Technologies
Project is created with:

* Java 1.8
* Gradle 6.5

Libraries:

* MaterialDesign 1.1.0
* Lombok 1.18.14

## Setup
To run this project, download and start it in android studio.

## Status
The project was made for learning purposes. It's not tested on real android device.

## Inspiration
This project was inspired by iOS weather app. Openweather API showed as a great tool to accomplish this goal.
